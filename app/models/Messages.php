<?php

namespace Models;

use Phalcon\Mvc\Model;

class Messages extends Model {

	/** @var  int */
	public $message_id;

	/** @var  int */
	public $chat_id;

	/** string */
	public $date;

	/** @var  string */
	public $message;

	/** @var  string */
	public $original_message;
}