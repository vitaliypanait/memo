<?php

use Phalcon\Cli\Task;

class SendTask extends Task {

	public function mainAction() {
		$this->db->execute("SET @@session.time_zone = '+00:00'");

		$messages = \Models\Messages::find([
			'date = :date:',
			'bind' => ['date' => (new DateTime())->format(FORMAT_DATE . ' H:i:00')]
		]);

		if ($messages->count() > 0) {
			$telegram = new \Telegram\Bot\Api($this->config->tg->secret_key);

			foreach ($messages as $message) {
				$telegram->sendMessage([
					'chat_id' => $message->chat_id,
					'text'    => $message->message
				]);
			}
		}
	}
}