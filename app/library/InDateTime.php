<?php

namespace Library;

class InDateTime extends DateAbstract implements DateInterface {

	public function getDate() {
		$dictionary = [
			['час', 'часов', 'часа'],
			['минуту', 'мин', 'минут', 'минуты']
		];

		$patternsValue = [];

		foreach ($dictionary as $items) {
			$patternsValue[] = '((\d+)?\s?(?:' . implode('|', $items) . '))?\s?';
		}

		preg_match('/^' . implode('', $patternsValue). '$/iu', $this->_dateData[1], $matches);

		if (empty($matches)) {
			throw new BadFormatException();
		}

		if (! empty($matches[3])) {
			$minutes = empty($matches[4]) ? 1 : (int) $matches[4];
			$minutes = strlen($minutes) === 1 ? "0$minutes" : $minutes;
		} else {
			$minutes = '00';
		}

		if (! empty($matches[1])) {
			$hours = empty($matches[2]) ? 1 : (int) $matches[2];
			$hours = strlen($hours) === 1 ? "$hours" : $hours;
		} else {
			$now = clone $this->_now;

			$tmpDateTime = $now->format("Y-m-d H:$minutes:00");
			$nowDateTime = $now->format(self::FORMAT_DATETIME_SECONDS);

			if ($tmpDateTime <= $nowDateTime) {
				$now->modify('+1 hours');
			}

			$hours = $now->format('H');
		}

		$time = "$hours:$minutes:00";

		$checkDate = $this->_now->format("Y-m-d $time");
		$date      = new \DateTime($checkDate);

		if ($checkDate <= $this->_now->format(self::FORMAT_DATETIME_SECONDS)) {
			$date->modify('+1 day');
		}

		return $date->format("Y-m-d $time");
	}
}