<?php

namespace Library;

interface DateInterface {
	public function getDate();
}