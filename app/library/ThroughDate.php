<?php

namespace Library;

class ThroughDate extends DateAbstract implements DateInterface {

	public function getDate() {
		$dictionary = [
			['месяц', 'месяцев', 'месяца'],
			['неделю', 'недели', 'недель'],
			['день', 'дней', 'дня'],
			['час', 'часов', 'часа'],
			['минуту', 'мин', 'минут', 'минуты']
		];

		$patternsValue = [];

		foreach ($dictionary as $items) {
			$patternsValue[] = '((\d+)?\s?(?:' . implode('|', $items) . '))?\s?';
		}

		preg_match('/^' . implode('', $patternsValue). '$/iu', $this->_dateData[1], $matches);

		if (empty($matches)) {
			throw new BadFormatException();
		}

		if (! empty($matches[1])) {
			$number = empty($matches[2]) ? 1 : (int) $matches[2];

			$this->_now->modify("+ $number month");
		}

		if (! empty($matches[3])) {
			$number = empty($matches[4]) ? 1 : (int) $matches[4];

			$this->_now->modify("+ $number week");
		}

		if (! empty($matches[5])) {
			$number = empty($matches[6]) ? 1 : (int) $matches[6];

			$this->_now->modify("+ $number day");
		}

		if (! empty($matches[7])) {
			$number = empty($matches[8]) ? 1 : (int) $matches[8];

			$this->_now->modify("+ $number hours");
		}

		if (! empty($matches[9])) {
			$number = empty($matches[10]) ? 1 : (int) $matches[10];

			$this->_now->modify("+ $number minutes");
		}

		return $this->_now->format('Y-m-d H:i:00');
	}
}