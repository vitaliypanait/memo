<?php

namespace Library;

class InDate extends DateAbstract implements DateInterface {

	public function getDate() {
		$dictionary = [
			'hours' => [
				'час',
				'часов',
				'часа'
			],
			'day' => [
				'дня'
			],
			'evening' => [
				'вечера'
			],
			'morning' => [
				'утра'
			]
		];

		$number = null;
		$type   = null;

		foreach ($dictionary as $key => $words) {
			preg_match('/^(\d+)?\s?(' . implode('|', $words) . ')$/iu', $this->_date , $matches);

			if (! empty($matches)) {
				$type   = $key;
				$number = count($matches) === 2 || empty($matches[1]) ? 1 : $matches[1];

				break;
			}
		}

		$endDay = $this->_now->format('Y-m-d') . ' ' . '23:59:59';


		if ($type === 'hours') {
			$checkTime = $this->_now->format("Y-m-d $number:00:00");

			if (strtotime($checkTime) <= strtotime($endDay) && strtotime($checkTime) >= strtotime($nowDateTime->format('Y-m-d H:i:s'))) {
				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			} else {
				$nowDateTime->modify('+1 day');

				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			}
		} else if ($type === 'day') {
			if ($number < 11) {
				$number += 12;
			}

			$checkTime = $nowDateTime->format("Y-m-d $number:00:00");

			if (strtotime($checkTime) >= strtotime($nowDateTime->format('Y-m-d H:i:s'))) {
				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			} else {
				$nowDateTime->modify('+1 day');

				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			}
		} else if ($type === 'evening') {
			if ($number <= 12) {
				$number += 12;
			}

			$checkTime = $nowDateTime->format("Y-m-d $number:00:00");

			if (strtotime($checkTime) >= strtotime($nowDateTime->format('Y-m-d H:i:s'))) {
				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			} else {
				$nowDateTime->modify('+1 day');

				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			}
		} else if ($type === 'morning') {
			if ($number > 12) {
				die('ok');
			}

			$checkTime = $nowDateTime->format("Y-m-d $number:00:00");

			if (strtotime($checkTime) >= strtotime($nowDateTime->format('Y-m-d H:i:s'))) {
				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			} else {
				$nowDateTime->modify('+1 day');

				$nowDateTime = new DateTime($nowDateTime->format("Y-m-d $number:00:00"));
			}
		}

		$this->_now->modify("+ $number $type");

		return $this->_now->format('Y-m-d H:i:00');
	}
}