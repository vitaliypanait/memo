<?php

namespace Library;

class FullDateTime extends DateAbstract implements DateInterface {

	public function getDate() {
		$date = (new \DateTime($this->_dateData[1], new \DateTimeZone($this->_offset)))->format('Y-m-d H:i:00');

		if ($this->_now->format(self::FORMAT_DATETIME_SECONDS) > $date) {
			throw new BadFormatException('Это событие уже произошло.');
		}

		return $date;
	}
}