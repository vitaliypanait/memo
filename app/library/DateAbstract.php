<?php

namespace Library;

abstract class DateAbstract {
	const FORMAT_DATETIME_SECONDS = 'Y-m-d H:i:00';

	const TIME_MIN = '00:00:00';
	const TIME_MAX = '23:59:59';

	protected $_dateData;

	protected $_now;

	protected $_offset;

	public function __construct($dateData, $offset) {
		$this->_dateData = $dateData;
		$this->_now      = new \DateTime('now', new \DateTimeZone($offset));
		$this->_offset   = $offset;

		return $this;
	}

	protected function _getTodayOrTomorrowDateByTime($time) {
		$time  = str_replace('.', ':', $time);
		$items = explode(':', $time);

		if (count($items) === 1) {
			$time .= ':00:00';
		} else if (count($items) === 2) {
			$time .= ':00';
		}

		$checkDate = $this->_now->format('Y-m-d ') . $time;
		$date      = new \DateTime($checkDate);

		if ($checkDate <= $this->_now->format(self::FORMAT_DATETIME_SECONDS)) {
			$date->modify('+1 day');
		}

		return $date->format(self::FORMAT_DATETIME_SECONDS);
	}

}