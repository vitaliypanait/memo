<?php

namespace Library;

class Parser {

	const PREFIX_THROUGH = 'через';
	const PREFIX_IN      = 'в';

	private $_dateTime;

	private $_text;

	private $_dictionary = [
		'час',
		'часов',
		'часа',
		'мин',
		'минут',
		'минута',
		'минуту',
		'минуты',
		'сек',
		'секунд',
		'секунда',
		'дней',
		'дня',
		'день',
		'месяц',
		'месяцев',
		'месяца',
		'утра',
		'вечера'
	];

	public function __construct($message, $offset) {
		$hasDate = false;
		$formats = [
			'InDateTime'       => [
				'/^' . self::PREFIX_IN . ' ((?:.+)?(?:' . implode('|', $this->_dictionary) . ')) (.*)$/iu'
			],
			'InTime'       => [
				'/^' . self::PREFIX_IN . ' (\d[\d]?([.:]\d[\d]?)?\2?) (.*)$/iu'
			],
			'FullDateTime' => [
				'/^(\d\d?.\d\в?\.\d{4} \d\d?[:]\d\d?(?::\d{2})?) (.*)$/iu',
				'/^(\d{4}-\d\d?-\d\d? \d\d?:\d\d?(?::\d{2})?) (.*)$/iu'
			],
			'ThroughDate' => [
				'/^' . self::PREFIX_THROUGH . ' ((?:.+)?(?:' . implode('|', $this->_dictionary) . ')) (.*)$/iu'
			]
		];

		foreach ($formats as $className => $regVariants) {

			foreach ($regVariants as $regex) {
				preg_match($regex, $message, $matches);

				if (! empty($matches)) {
					$this->_text = trim(urldecode(array_pop($matches)));

					$className = "\\Library\\$className";

					$date = new $className($matches, $offset);

					$this->_dateTime = $date->getDate();

					$hasDate = true;

					break 2;
				}
			}
		}

		if (! $hasDate) {
			throw new BadFormatException();
		}
	}


	public function getText() {
		return $this->_text;
	}

	public function getDateTime() {
		return $this->_dateTime;
	}

}