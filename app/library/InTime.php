<?php

namespace Library;

class InTime extends DateAbstract implements DateInterface {

	public function getDate() {
		return $this->_getTodayOrTomorrowDateByTime($this->_dateData[1]);
	}
}