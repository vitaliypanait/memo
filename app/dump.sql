CREATE DATABASE memo
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

CREATE TABLE IF NOT EXISTS `messages` (
  `message_id` INT(11)    UNSIGNED NOT NULL AUTO_INCREMENT,
  `chat_id`    INT(11)    UNSIGNED NOT NULL DEFAULT 0,
  `date`       TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `message`    TEXT                         DEFAULT NULL,
  PRIMARY KEY (`message_id`)
)
  ENGINE=InnoDB
  DEFAULT CHARSET=utf8
  COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `chats` (
  `chat_id`     INT(11)    UNSIGNED NOT NULL,
  `first_name`  VARCHAR(256)        DEFAULT NULL,
  `last_name`   VARCHAR(256)        DEFAULT NULL,
  `offset`      TINYINT(1) UNSIGNED DEFAULT NULL,
  `created`     TIMESTAMP           NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`chat_id`)
)
  ENGINE=InnoDB
  DEFAULT CHARSET=utf8
  COLLATE=utf8_unicode_ci;