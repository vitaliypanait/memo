<?php

use Phalcon\Loader;

try {
	require __DIR__ . '/vendor/autoload.php';
	require 'constants.php';

	date_default_timezone_set('UTC');

	$di = new \Phalcon\Di\FactoryDefault\Cli();

	/**
	 * Регистрируем автозагрузчик, и скажем ему, чтобы зарегистрировал каталог задач
	 */
	$loader = new Loader();

	$loader->registerNamespaces([
		'Models'  => __DIR__ . "/app/models/",
		'Library' => __DIR__ . "/app/library/"
	]);

	$loader->registerDirs([__DIR__ . "/app/tasks/"]);

	$loader->register();

	require 'bootstrap/services.php';

	$console = new \Phalcon\Cli\Console();

	$console->setDI($di);

	$arguments = [];

	foreach ($argv as $k => $arg) {
		if ($k === 1) {
			$arguments["task"] = $arg;
		} elseif ($k === 2) {
			$arguments["action"] = $arg;
		} elseif ($k >= 3) {
			$arguments["params"][] = $arg;
		}
	}

	$console->handle($arguments);
} catch (\Exception $e) {
	throw $e;
}

echo PHP_EOL;
return 0;