<?php

const TIME_MINUTE = 60;
const TIME_HOUR   = 3600;
const TIME_DAY    = 86400;

const TIME_MIN = '00:00:00';
const TIME_MAX = '23:59:59';

const FORMAT_DATETIME         = 'Y-m-d H:i:s';
const FORMAT_DATE             = 'Y-m-d';

const CONF_PATH   = __DIR__ . '/app/config';