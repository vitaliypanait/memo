<?php

use Phalcon\Mvc\Micro;

try {
	require __DIR__ . '/vendor/autoload.php';
	require 'constants.php';

	date_default_timezone_set('Europe/Moscow');

	$loader = new \Phalcon\Loader();

	$loader->registerNamespaces([
		'Models'   => __DIR__ . "/app/models/",
		'Library'  => __DIR__ . "/app/library/",
	]);

	$loader->register();

	$di  = new \Phalcon\Di\FactoryDefault();
	$app = new Micro($di);

	require 'bootstrap/services.php';

//	$app->get('/api/webhook', function() use ($app) {
//		$telegram = new \Telegram\Bot\Api($this->config->tg->secret_key);
//		$response = $telegram->setWebhook(['url' => 'https://a489d8d1.ngrok.io/api/add/']);
//
//		var_dump($response); die;
//	});

	$app->post(
		'/api/add',
		function() use ($app) {
			$telegram = new \Telegram\Bot\Api($this->config->tg->secret_key);
			$updates  = $telegram->getWebhookUpdates()->getMessage();

			if ($updates === null) {
				return;
			}

			$chat = $updates->getChat();
			$user = \Models\Chats::findFirst($chat->getId());
			$text = $updates->getText();

			try {
				switch ($text) {
					case '/start' :
						if (! $user) {
							$user = new \Models\Chats([
								'chat_id'    => $chat->getId(),
								'first_name' => $chat->getFirstName(),
								'last_name'  => $chat->getLastName()
							]);

							if (! $user->create()) {
								throw new \Exception('Error to create new user');
							}

							$message = implode(PHP_EOL, [
								'Привет, ' . $chat->getUsername() . '. Я помогу тебе не забыть.',
								'Укажи смещение времени относительно UTC. Например, если ты находишься в Москве, то твое смещение будет +3'
							]);

							$telegram->sendMessage([
								'chat_id' => $chat->getId(),
								'text'    => $message
							]);
						}

						break;

					case '/help' :
						$telegram->sendMessage([
							'chat_id' => $chat->getId(),
							'text'    => 'Я помогу тебе вспомнить.'
						]);

						break;

					default :
						if (! $user) {
							$telegram->sendMessage([
								'chat_id' => $chat->getId(),
								'text'    => 'Для начала работы с ботом отправь сообщение /start'
							]);

							return;
						} else if (empty($user->offset)) {
							preg_match('/^\+(\d{1,2})$/iu', $text, $matches);

							if (! empty($matches[1])) {
								$user->offset = $matches[1];

								$user->save();

								$telegram->sendMessage([
									'chat_id' => $chat->getId(),
									'text'    => 'Спасибо. Теперь можешь напоминать себе.'
								]);
							} else {
								$telegram->sendMessage([
									'chat_id' => $chat->getId(),
									'text'    => 'Укажи смещение времени относительно UTC. Например, если ты находишься в Москве, то твое смещение будет +3'
								]);
							}
						} else {
							$this->db->execute("SET @@session.time_zone = '{$user->getOffset()}:00'");

							$parser  = new \Library\Parser($text, $user->getOffset());
							$message = new \Models\Messages([
								'chat_id'          => $user->chat_id,
								'message'          => $parser->getText(),
								'date'             => $parser->getDateTime(),
								'original_message' => $parser->getText()
							]);

							if (! $message->create()) {
								throw new \Exception('Error to create new message');
							}

							$telegram->sendMessage([
								'chat_id' => $chat->getId(),
								'text'    => $parser->getDateTime() . ' ' . $parser->getText()
							]);
						}
				}
			} catch (\Library\BadFormatException $e) {
				$telegram->sendMessage([
					'chat_id' => $updates->getChat()->getId(),
					'text'    => empty($e->getMessage()) ? 'Я тебя не понял. Попробуй еше раз.' : $e->getMessage()
				]);
			} catch (\Exception $e) {
				/** @todo Add log and notify for admin */

				$telegram->sendMessage([
					'chat_id' => 86860298,
					'text'    => $e->getMessage()
				]);

				throw $e;
			}
		}
	);

	$app->notFound(
		function () use ($app) {
			$app->response->setStatusCode(404, "Not Found");

			$app->response->sendHeaders();

			echo "Page was not found!";
		}
	);

	$app->handle();
} catch (\Exception $e) {
	throw $e;
}