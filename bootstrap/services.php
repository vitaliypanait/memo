<?php

$di->set(
	'config',
	function () {
		$config      = include CONF_PATH . '/settings.php';
		$localConfig = include CONF_PATH . '/config.php';

		return $config->merge($localConfig);
	}
);

$di->set(
	'db',
	function () use ($di) {
		return new \Phalcon\Db\Adapter\Pdo\Mysql($di->get('config')->db->toArray());
	}
);